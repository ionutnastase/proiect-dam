package ratingapp.ddey.com.dam_project.activities.wip_activities.wip_student;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TableLayout;

import java.util.ArrayList;
import java.util.List;

import ratingapp.ddey.com.dam_project.R;
import ratingapp.ddey.com.dam_project.activities.AbstractActivity;

public class QuizzHistoryActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz_history);
        super.onCreateDrawer();
        populateHistory();
    }

    protected void populateHistory(){
        TableLayout layout=findViewById(R.id.history_tl);
        ListView listas=new ListView(this);
        List<String> lista=new ArrayList<>();
        lista.add("Ionut");
        lista.add("Andrei");
        //de facut adaptor custom, am creat in java->models -> HistoryDataModel
        //ArrayAdapter ad=new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1,lista);
        //listas.setAdapter(ad);

        layout.addView(listas);


    }

//    protected void populateHistory(){
//        TableLayout tl=findViewById(R.id.history_tl);
//
//        TableRow headerTabel=findViewById(R.id.tl_history_headerRow);
//        TextView quizzName=findViewById(R.id.tl_history_headerQuizzName);
//        TextView quizzNr=findViewById(R.id.tl_history_headerNumber);
//        TextView quizzResult=findViewById(R.id.tl_history_headerQuizzResult);
//        List<Integer> nr=new ArrayList<Integer>();
//        List<String> nume=new ArrayList<String>();
//        nume.add("Ionut");
//        nume.add("Andrei");
//        nume.add("Sebi");
//        nume.add("Roxana");
//        nume.add("Andreea");
//        for(Integer i=1;i<=4;i++){
//            nr.add(i);
//        }
//
//
//        for(Integer i:nr){
//            TableRow newLine=new TableRow(this);
//            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
//            newLine.setLayoutParams(lp);
//            quizzNr.setText("i");
//            quizzName.setText(nume.get(i));
//            quizzResult.setText(i*2+80);
//            newLine.addView(quizzNr);
//            newLine.addView(quizzName);
//            newLine.addView(quizzResult);
//            tl.addView(newLine);
//        }
//    }
}
