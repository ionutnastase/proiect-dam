package ratingapp.ddey.com.dam_project.utils;

import java.text.SimpleDateFormat;

public interface Constants {
    String DATE_FORMAT = "dd-MM-yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
    String ADD_QUESTION_KEY = "newQuestion";
    String ADD_QUIZZ_KEY = "newQuizz";
    String ACTIVATE_QUIZZ_KEY = "toActive";
    String MODIFY_QUIZZ_KEY = "toModify";
    int REQUEST_CODE_ADD_QUESTION = 2;
    int REQUEST_CODE_CREATE_QUIZZ = 3;
    int REQUEST_CODE_ACTIVATE_QUIZZ = 4;
    int REQUEST_CODE_MODIFY_QUIZZ = 5;
}
