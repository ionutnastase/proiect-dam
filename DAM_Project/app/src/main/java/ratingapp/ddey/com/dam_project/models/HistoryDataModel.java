package ratingapp.ddey.com.dam_project.models;

public class HistoryDataModel {

    String name;
    Integer nr;
    Double Score;

    public HistoryDataModel(String name, Integer nr, Double score) {
        this.name = name;
        this.nr = nr;
        Score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNr() {
        return nr;
    }

    public void setNr(Integer nr) {
        this.nr = nr;
    }

    public Double getScore() {
        return Score;
    }

    public void setScore(Double score) {
        Score = score;
    }


}