package ratingapp.ddey.com.dam_project.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Quizz implements Parcelable {
    private int idQuizz;
    private String title;
    private String description;
    private String visibility;
    private long accessCode;
    private List<Question> questionsList;
    private String category;

    public Quizz() {

    }

    public Quizz(String title, String description, String visibility) {
        this.title = title;
        this.description = description;
        this.visibility = visibility;
    }

    public int getIdQuizz() {
        return idQuizz;
    }

    public void setIdQuizz(int idQuizz) {
        this.idQuizz = idQuizz;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public long getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(long accessCode) {
        this.accessCode = accessCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Quizz> CREATOR = new Creator<Quizz>() {
        @Override
        public Quizz createFromParcel(Parcel in) {
            return new Quizz(in);
        }

        @Override
        public Quizz[] newArray(int size) {
            return new Quizz[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idQuizz);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(visibility);
        dest.writeLong(accessCode);
        dest.writeTypedList(questionsList);
    }

    protected Quizz(Parcel in) {
        idQuizz = in.readInt();
        title = in.readString();
        description = in.readString();
        visibility = in.readString();
        accessCode = in.readLong();
        questionsList = in.createTypedArrayList(Question.CREATOR);
    }

    @Override
    public String toString() {
        return "Quizz{" +
                "idQuizz=" + idQuizz +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", visibility='" + visibility + '\'' +
                ", accessCode=" + accessCode +
                ", questionsList=" + questionsList +
                ", category='" + category + '\'' +
                '}';
    }
}
