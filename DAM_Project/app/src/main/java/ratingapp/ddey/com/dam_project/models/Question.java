package ratingapp.ddey.com.dam_project.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Question implements Parcelable {
    private static int idQuestion = 0;
    private String questionText;
    private int answerTime;
    private List<String> answersList;


    public Question(String questionText, int answerTime, List<String> answersList) {
        this.questionText = questionText;
        this.answerTime = answerTime;
        this.answersList = answersList;
        idQuestion++;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public int getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(int answerTime) {
        this.answerTime = answerTime;
    }

    public List<String> getAnswersList() {
        return answersList;
    }

    public void setAnswersList(List<String> answersList) {
        this.answersList = answersList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idQuestion);
        dest.writeString(questionText);
        dest.writeInt(answerTime);
        dest.writeStringList(answersList);
    }


    protected Question(Parcel in) {
        idQuestion = in.readInt();
        questionText = in.readString();
        answerTime = in.readInt();
        answersList = in.createStringArrayList();
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
}
