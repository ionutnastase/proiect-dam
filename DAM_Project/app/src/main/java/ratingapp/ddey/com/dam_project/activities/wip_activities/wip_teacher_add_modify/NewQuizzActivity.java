package ratingapp.ddey.com.dam_project.activities.wip_activities.wip_teacher_add_modify;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ratingapp.ddey.com.dam_project.R;
import ratingapp.ddey.com.dam_project.models.Question;
import ratingapp.ddey.com.dam_project.models.Quizz;
import ratingapp.ddey.com.dam_project.utils.Constants;
import ratingapp.ddey.com.dam_project.utils.QuestionAdapter;

public class NewQuizzActivity extends AppCompatActivity {

    private TextInputEditText tieTitle;
    private TextInputEditText tieDescription;
    private Spinner spnVisibility;
    private ListView lvQuestions;
    private Button btnCreate;
    private FloatingActionButton btnAdd;

    private List<Question> questionList;
    private QuestionAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_quizz);
        init();
    }

    private void init() {
        tieTitle = findViewById(R.id.newquizz_tie_title);
        tieDescription = findViewById(R.id.newquizz_tie_description);
        spnVisibility = findViewById(R.id.newquizz_spn_visibility);
        btnCreate = findViewById(R.id.newquizz_btn_create);
        btnAdd = findViewById(R.id.newquizz_btn_addquestion);
        lvQuestions = findViewById(R.id.newquizz_lv_questions);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.newquizz_spn_visibility, R.layout.support_simple_spinner_dropdown_item);
        spnVisibility.setAdapter(adapter);
        spnVisibility.setSelection(0);

        questionList = new ArrayList<>();
        List<String> hardcodedAnswers = new ArrayList<>();
        hardcodedAnswers.add("an2");
        hardcodedAnswers.add("an3");
        questionList.add(new Question("HardcodedQuestionText", 20, hardcodedAnswers));
        mAdapter = new QuestionAdapter(getApplicationContext(), R.layout.lv_questions_row, questionList);
        lvQuestions.setAdapter(mAdapter);

        btnAdd.setOnClickListener(addEvent());
        btnCreate.setOnClickListener(saveEvent());
    }

    public View.OnClickListener addEvent() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewQuestionActivity.class);
                startActivityForResult(intent, Constants.REQUEST_CODE_ADD_QUESTION);
            }
        };
    }

    public View.OnClickListener saveEvent() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValid()) {

                    String title = tieTitle.getText().toString();
                    String description = tieDescription.getText().toString();
                    String visibility = spnVisibility.getSelectedItem().toString();
                    Quizz newQuizz = new Quizz(title, description, visibility);

                    Intent returnIntent = getIntent();
                    returnIntent.putExtra(Constants.ADD_QUIZZ_KEY, newQuizz);
                    setResult(RESULT_OK, returnIntent);

                    Toast.makeText(getApplicationContext(), "Quizz created! You can find it in the inactive quizzes list to activate it successfully", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Some error occured! You can't save the quizz!", Toast.LENGTH_SHORT).show();
                }

            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE_ADD_QUESTION && resultCode == RESULT_OK && data != null) {
            Question receivedQuestion = data.getParcelableExtra(Constants.ADD_QUESTION_KEY);

            if (receivedQuestion != null) {
                Toast.makeText(getApplicationContext(), "Question added successfully", Toast.LENGTH_SHORT).show();
                questionList.add(receivedQuestion);
                mAdapter.notifyDataSetChanged();
            }
        } else {
            {
                Toast.makeText(getApplicationContext(), "Question wasn't added!",
                        Toast.LENGTH_LONG).show();
            }
        }
    }


    private boolean isValid() {
        if (tieTitle.getText() == null || tieTitle.getText().toString().trim().isEmpty()) {
            tieTitle.setError("Inserting a title is mandatory!");
            tieTitle.requestFocus();
            return false;
        } else if (tieDescription.getText() == null || tieDescription.getText().toString().trim().isEmpty()) {
            tieDescription.setError("Inserting a description here is mandatory");
            tieDescription.requestFocus();
            return false;
        } else if (questionList.size() < 1) {
            Toast.makeText(getApplicationContext(), "You can't create a quizz with less than one question!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
