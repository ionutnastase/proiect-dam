package ratingapp.ddey.com.dam_project.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ratingapp.ddey.com.dam_project.R;

public class AboutActivity extends AbstractActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        super.onCreateDrawer();
    }
}
