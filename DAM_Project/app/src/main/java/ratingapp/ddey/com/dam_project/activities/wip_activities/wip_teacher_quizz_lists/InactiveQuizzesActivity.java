package ratingapp.ddey.com.dam_project.activities.wip_activities.wip_teacher_quizz_lists;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ratingapp.ddey.com.dam_project.R;
import ratingapp.ddey.com.dam_project.models.Quizz;
import ratingapp.ddey.com.dam_project.utils.Constants;
import ratingapp.ddey.com.dam_project.utils.QuizzAdapter;

public class InactiveQuizzesActivity extends AppCompatActivity {
    private static List<Quizz> quizzList = new ArrayList<>();

    private ListView lvQuizzes;

    private QuizzAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inactive_quizzes);
        init();
        intentIncoming();
    }

    public void init() {
        lvQuizzes = findViewById(R.id.lv_inactive_quizzes);

        quizzList.add(new Quizz("Hardcoded:Title", "Hardcoded:Description", "Hardcoded:Visibility"));
        mAdapter = new QuizzAdapter(getApplicationContext(), R.layout.lv_quizz_row, quizzList, this);
        lvQuizzes.setAdapter(mAdapter);

        lvQuizzes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Quizz currentQuizz = (Quizz)parent.getItemAtPosition(position);
                Intent intent = new Intent(getApplicationContext(), ActivateQuizzActivity.class);
                intent.putExtra("toActive", currentQuizz);
                startActivity(intent);
            }
        });
    }

    public void intentIncoming() {
        Intent intent = getIntent();
        if (intent != null) {
            Quizz receivedQuizz = intent.getParcelableExtra(Constants.ADD_QUIZZ_KEY);

            if (receivedQuizz != null) {
                quizzList.add(receivedQuizz);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == Constants.REQUEST_CODE_ACTIVATE_QUIZZ && data != null) {
            if (resultCode == RESULT_OK) {
                Quizz recvQuizz = data.getParcelableExtra(Constants.ACTIVATE_QUIZZ_KEY);
                quizzList.remove(recvQuizz);

                if (recvQuizz != null) {
                    Intent intent = new Intent(getApplicationContext(), ActiveQuizzesActivity.class);
                    intent.putExtra(Constants.ACTIVATE_QUIZZ_KEY, recvQuizz);
                    startActivity(intent);
                    finish();
                }
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "No changes made to the selected list item!", Toast.LENGTH_SHORT).show();
            }
        }
        //posibil problema
        if (requestCode == Constants.REQUEST_CODE_MODIFY_QUIZZ && data != null) {
            if (resultCode == RESULT_OK) {
                int position = data.getIntExtra("position", -1);
                Quizz currentQuizz = quizzList.get(position);
                currentQuizz = data.getParcelableExtra(Constants.MODIFY_QUIZZ_KEY);
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "No changes made to the selected list item!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
