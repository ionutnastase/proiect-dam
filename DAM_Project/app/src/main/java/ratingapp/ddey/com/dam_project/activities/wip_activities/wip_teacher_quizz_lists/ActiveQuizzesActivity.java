package ratingapp.ddey.com.dam_project.activities.wip_activities.wip_teacher_quizz_lists;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ratingapp.ddey.com.dam_project.R;
import ratingapp.ddey.com.dam_project.models.Quizz;
import ratingapp.ddey.com.dam_project.utils.Constants;

public class ActiveQuizzesActivity extends AppCompatActivity {
    private static List<Quizz> activeQuizzList = new ArrayList<>();

    private TextView test;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_quizzes);

        test = findViewById(R.id.test);
        incomingIntent();
    }

    public void incomingIntent() {
        Intent intent = getIntent();
        Quizz recvQuizz = intent.getParcelableExtra(Constants.ACTIVATE_QUIZZ_KEY);
        if (recvQuizz != null) {
            activeQuizzList.add(recvQuizz);
            if (recvQuizz.getTitle() != null) {
                test.setText(recvQuizz.getTitle());
            }
        }
    }
}
