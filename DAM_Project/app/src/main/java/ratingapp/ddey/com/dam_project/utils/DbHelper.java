package ratingapp.ddey.com.dam_project.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import ratingapp.ddey.com.dam_project.activities.EditProfileActivity;
import ratingapp.ddey.com.dam_project.models.User;
import ratingapp.ddey.com.dam_project.models.UserType;


public class DbHelper extends SQLiteOpenHelper {
        public static final String TAG = DbHelper.class.getSimpleName();
        public static final String DB_NAME = "DAM_DATABASE.db";
        public static final int DB_VERSION = 1;

        //USERS
        public static final String USER_TABLE = "users";
        public static final String COLUMN_ID_USER = "id_user";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_PASS = "password";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_GENDER = "gender";
        public static final String COLUMN_USER_TYPE = "user_type";

        //NOTES
        public static final String NOTE_TABLE = "notes";
        public static final String COLUMN_ID_NOTE = "id_note";
        public static final String COLUMN_NAME_NOTE = "name";

        public static final String CREATE_TABLE_USERS = "CREATE TABLE " + USER_TABLE + "("
                + COLUMN_ID_USER + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_EMAIL + " TEXT NOT NULL,"
                + COLUMN_NAME + " TEXT NOT NULL,"
                + COLUMN_PASS + " TEXT NOT NULL,"
                + COLUMN_USER_TYPE + " TEXT NOT NULL,"
                + COLUMN_DATE + " TEXT,"
                + COLUMN_GENDER + " TEXT);";

        public static final String CREATE_TABLE_NOTES = "CREATE TABLE " + NOTE_TABLE + "("
                + COLUMN_ID_NOTE + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_NAME_NOTE + " TEXT NOT NULL,"
                + COLUMN_ID_USER + " INTEGER, "
                + "FOREIGN KEY (" + COLUMN_ID_USER + ") REFERENCES " + USER_TABLE + "(" + COLUMN_ID_USER +"));";


        public DbHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_USERS);
            db.execSQL(CREATE_TABLE_NOTES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + NOTE_TABLE);
            onCreate(db);
        }

        public void insertUser(User user) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_EMAIL, user.getEmail());
            values.put(COLUMN_NAME, user.getName());
            values.put(COLUMN_PASS, user.getPassword());
            values.put(COLUMN_USER_TYPE, user.getUserType().name());
            Log.d(TAG, "insertUser: " + user.getUserType().name());
            values.put(COLUMN_DATE, "");
            values.put(COLUMN_GENDER, "");

            long id = db.insert(USER_TABLE, null, values);
            db.close();
            Log.d(TAG, "User inserted " + id);
        }


        public void insertNote(String noteString, Session session) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_NOTE, noteString);
            values.put(COLUMN_ID_USER, getId(session));
            long id = db.insert(NOTE_TABLE, null, values);
            db.close();

            Log.d(TAG, "note inserted " + id);
        }

        public void deleteNote(String noteString) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(NOTE_TABLE, COLUMN_NAME_NOTE + " = ?", new String[]{noteString});
            db.close();
        }

        public ArrayList<String> getNotesList(Session session) {
            ArrayList<String> resultList = new ArrayList<>();
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.query(NOTE_TABLE,new String[]{COLUMN_NAME_NOTE},   COLUMN_ID_USER + " = ?", new String[]{getId(session)}, null, null, null);
            while (c.moveToNext()) {
                int index = c.getColumnIndex(COLUMN_NAME_NOTE);
                resultList.add(c.getString(index));
            }
            c.close();
            db.close();

            return resultList;
        }

        public boolean getUser(User user) {
            String selectQuery = "SELECT * FROM " + USER_TABLE + " WHERE "
                    + COLUMN_EMAIL + " = " + "'" + user.getEmail() + "'" + " and "
                    + COLUMN_PASS + " = " + "'" + user.getPassword() + "'";
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0)
                return true;

            cursor.close();
            db.close();

            return false;
        }

        public int getUserId(User user)  {
            String selectQuery = "SELECT " + COLUMN_ID_USER + " FROM " + USER_TABLE + " WHERE "
                    + COLUMN_EMAIL + " = " + "'" + user.getEmail() + "'" + " and "
                    + COLUMN_PASS + " = " + "'" + user.getPassword() + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            int result = cursor.getInt(0);

            cursor.close();
            db.close();

            return result;
        }

        public String getUserType(Session session) {
            HashMap<String, String> user = session.getUserDetails();
            String email = user.get(Session.KEY_EMAIL);
            String pass = user.get(Session.KEY_PASS);

            String selectQuery = "SELECT " + COLUMN_USER_TYPE + " FROM " + USER_TABLE + " WHERE "
                    + COLUMN_EMAIL + " = " + "'" + email + "'" + " and "
                    + COLUMN_PASS + " = " + "'" + pass + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            String rez = "";
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0)
            {
                rez = cursor.getString(cursor.getColumnIndex(COLUMN_USER_TYPE));
            }

            cursor.close();
            db.close();

            return rez;
        }

    public String getName(Session session) {
        HashMap<String, String> user = session.getUserDetails();
        String email = user.get(Session.KEY_EMAIL);
        String pass = user.get(Session.KEY_PASS);

        String selectQuery = "SELECT name FROM " + USER_TABLE + " WHERE "
                + COLUMN_EMAIL + " = " + "'" + email + "'" + " and "
                + COLUMN_PASS + " = " + "'" + pass + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        String rez = "";
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0)
        {
            rez = cursor.getString(cursor.getColumnIndex("name"));
        }

        cursor.close();
        db.close();

        return rez;
    }

        public String getId(Session session) {
            HashMap<String, String> user = session.getUserDetails();
            String email = user.get(Session.KEY_EMAIL);
            String pass = user.get(Session.KEY_PASS);

            String selectQuery = "SELECT " + COLUMN_ID_USER + " FROM " + USER_TABLE + " WHERE "
                    + COLUMN_EMAIL + " = " + "'" + email + "'" + " and "
                    + COLUMN_PASS + " = " + "'" + pass + "'";

            SQLiteDatabase db = getReadableDatabase();
            String rez = "";
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            int result = cursor.getInt(0);
            rez = String.valueOf(result);
            cursor.close();

            return rez;
        }

        public void updateEmail(String id, String email) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_EMAIL, email);

            db.update(USER_TABLE, values, COLUMN_ID_USER + " = " + id, null);
            db.close();
        }

        public void updateDate(String id, String birthDate) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_DATE, birthDate);
            db.update(USER_TABLE, values, COLUMN_ID_USER + " = " + id, null);
            db.close();
        }

        public User retrieveProfile(Session session) {
            ArrayList<User> list = new ArrayList<>();
            SQLiteDatabase db = this.getReadableDatabase();

            HashMap<String, String> user = session.getUserDetails();
            String email = user.get(Session.KEY_EMAIL);
            String pass = user.get(Session.KEY_PASS);
            String selectQuery = "SELECT * FROM " + USER_TABLE + " WHERE "
                    + COLUMN_EMAIL + " = " + "'" + email + "'" + " and "
                    + COLUMN_PASS + " = " + "'" + pass + "'";

            Cursor cursor = db.rawQuery(selectQuery, null);
            User userRetrieved = new User();
            if (cursor.moveToFirst())
            {
                do {
                    userRetrieved.setEmail(cursor.getString(1));
                    userRetrieved.setName(cursor.getString(2));
                    userRetrieved.setPassword(cursor.getString(3));
                    userRetrieved.setUserType(UserType.valueOf(cursor.getString(4)));
                    userRetrieved.setDate(EditProfileActivity.convertStringToDate(cursor.getString(5)));
                    userRetrieved.setGender(cursor.getString(6));

                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();

            return userRetrieved;
        }

        public void updateGender(String id, String gender) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_GENDER, gender);
            db.update(USER_TABLE, values, COLUMN_ID_USER + " = " + id, null);
            db.close();
        }
}

