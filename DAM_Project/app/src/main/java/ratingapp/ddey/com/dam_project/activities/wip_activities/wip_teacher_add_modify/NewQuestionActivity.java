package ratingapp.ddey.com.dam_project.activities.wip_activities.wip_teacher_add_modify;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ratingapp.ddey.com.dam_project.R;
import ratingapp.ddey.com.dam_project.models.Question;
import ratingapp.ddey.com.dam_project.utils.Constants;

public class NewQuestionActivity extends AppCompatActivity {
    private Spinner spnDuration;
    private TextInputEditText tieQuestion;
    private TextInputEditText tieAnswer1;
    private TextInputEditText tieAnswer2;
    private TextInputEditText tieAnswer3;
    private TextInputEditText tieAnswer4;
    private Button btnAdd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_question);
        init();
    }

    private void init() {
        spnDuration = findViewById(R.id.newquestion_spn_duration);
        tieQuestion = findViewById(R.id.newquestion_tie_questiontext);
        tieAnswer1 = findViewById(R.id.newquestion_tie_answer1);
        tieAnswer2 = findViewById(R.id.newquestion_tie_answer2);
        tieAnswer3 = findViewById(R.id.newquestion_tie_answer3);
        tieAnswer4 = findViewById(R.id.newquestion_tie_answer4);
        btnAdd = findViewById(R.id.newquestion_btn_add);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.newquestion_spn_duration, R.layout.support_simple_spinner_dropdown_item);
        spnDuration.setAdapter(adapter);

        spnDuration.setSelection(0);
        btnAdd.setOnClickListener(saveEvent());
    }


    public View.OnClickListener saveEvent() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValid()) {

                    String questionText = tieQuestion.getText().toString();
                    int answerTime = Integer.valueOf(spnDuration.getSelectedItem().toString().substring(0,3).trim());

                    List<String> answersList = new ArrayList<>();
                    answersList.add(tieAnswer1.getText().toString());
                    answersList.add(tieAnswer2.getText().toString());
                    if (tieAnswer3.getText() != null && !tieAnswer3.getText().toString().trim().isEmpty())
                        answersList.add(tieAnswer3.getText().toString());
                    if (tieAnswer4.getText() != null && !tieAnswer4.getText().toString().trim().isEmpty())
                        answersList.add(tieAnswer3.getText().toString());

                    Question newQuestion = new Question(questionText, answerTime, answersList);

                    Intent returnIntent = getIntent();
                    returnIntent.putExtra(Constants.ADD_QUESTION_KEY, newQuestion);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            }
        };
    }


    private boolean isValid() {
            if (tieQuestion.getText() == null || tieQuestion.getText().toString().trim().isEmpty()) {
                tieQuestion.setError("Inserting a question is mandatory!");
                tieQuestion.requestFocus();
                return false;
            } else if (tieAnswer1.getText() == null || tieAnswer1.getText().toString().trim().isEmpty()) {
                tieAnswer1.setError("Inserting an answer here is mandatory");
                tieAnswer1.requestFocus();
                return false;
            } else if (tieAnswer2.getText() == null || tieAnswer2.getText().toString().trim().isEmpty()) {
                tieAnswer2.setError("Inserting an answer here is mandatory");
                tieAnswer2.requestFocus();
                return false;
            }
            return true;
    }
}
