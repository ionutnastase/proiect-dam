package ratingapp.ddey.com.dam_project.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import ratingapp.ddey.com.dam_project.R;
import ratingapp.ddey.com.dam_project.activities.wip_activities.wip_teacher_add_modify.ModifyQuizzActivity;
import ratingapp.ddey.com.dam_project.activities.wip_activities.wip_teacher_quizz_lists.ActivateQuizzActivity;
import ratingapp.ddey.com.dam_project.models.Quizz;

public class QuizzAdapter extends ArrayAdapter<Quizz> {
    private Context mContext;
    private int mResource;
    private List<Quizz> mList;
    private Activity mActivity;

    public QuizzAdapter(@NonNull Context context, int resource, List<Quizz> list, Activity activity) {
        super(context, resource, list);
        this.mContext = context;
        this.mResource = resource;
        this.mList = list;
        this.mActivity = activity;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater  = LayoutInflater.from(mContext);
            view = inflater.inflate(mResource, null);
        }

        Quizz quizz = getItem(position);

        if (quizz != null) {
            TextView tvTitle = view.findViewById(R.id.lv_quizz_title);
            TextView tvDescription = view.findViewById(R.id.lv_quizz_description);
            TextView tvVisibility = view.findViewById(R.id.lv_quizz_visibility);

            if (quizz.getTitle() != null) {
                tvTitle.setText(quizz.getTitle());
            }
            if (quizz.getDescription() != null) {
                tvDescription.setText(quizz.getDescription());
            }
            if (quizz.getVisibility() != null) {
                tvVisibility.setText(quizz.getVisibility());
            }
        }

        ImageButton deleteButton = view.findViewById(R.id.lv_inactivequizz_imgbutton_delete);
        deleteButton.setTag(position);

        deleteButton.setOnClickListener(
                new ImageButton.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int index = (int) v.getTag();
                        mList.remove(index);
                        notifyDataSetChanged();
                    }
                }
        );

        Button btnActivate = view.findViewById(R.id.lv_inactive_quizzes_btn_activate);
        btnActivate.setTag(position);

        btnActivate.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = (int) v.getTag();
                Quizz currentQuizz = mList.get(index);
                Intent intent = new Intent(mContext, ActivateQuizzActivity.class);
                intent.putExtra(Constants.ACTIVATE_QUIZZ_KEY, currentQuizz);
                mActivity.startActivityForResult(intent, Constants.REQUEST_CODE_ACTIVATE_QUIZZ);
            }
        });

        ImageButton settingsButton = view.findViewById(R.id.lv_inactivequizz_imgbutton_settings);
        settingsButton.setTag(position);

        settingsButton.setOnClickListener(
                new ImageButton.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int index = (int) v.getTag();
                        Quizz currentQuizz = mList.get(index);
                        Intent intent = new Intent(mContext, ModifyQuizzActivity.class);
                        intent.putExtra(Constants.MODIFY_QUIZZ_KEY, currentQuizz);
                        intent.putExtra("position", index);
                        mActivity.startActivityForResult(intent, Constants.REQUEST_CODE_MODIFY_QUIZZ);
                    }
                }
        );

        return view;
    }


}
