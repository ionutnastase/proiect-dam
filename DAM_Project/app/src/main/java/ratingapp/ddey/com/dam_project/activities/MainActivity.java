package ratingapp.ddey.com.dam_project.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ratingapp.ddey.com.dam_project.R;
import ratingapp.ddey.com.dam_project.utils.DbHelper;
import ratingapp.ddey.com.dam_project.utils.Session;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class MainActivity extends AbstractActivity {
    private TextView tvMessage;

    private Session mSession;
    private DbHelper mDb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        super.onCreateDrawer();

        mSession = new Session(this);
        mDb = new DbHelper(this);

        tvMessage = findViewById(R.id.main_tv_user);

        tvMessage.setText("Hello " + mDb.getName(mSession) + "!");
    }
}
